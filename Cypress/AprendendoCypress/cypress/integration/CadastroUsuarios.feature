
Feature: Cadastro de usuarios no site Aprendendo a testar 

    Com diferentes tipos de cenários de login
    Eu quero validar os cenários abaixo


    Scenario Outline: Cadastrar usuario com login e senha validos
        Given acesso a pagina de cadastro de usuarios
        When preencho o usuario <USUARIO> e nome <NOME> e senha <SENHA>
        And clico no botao Enviar
        Then devo visualizar o usuario <USUARIO> na lista de usuarios cadastrados

        Examples:
        | USUARIO       | NOME           | SENHA  |
        | "joaoClaudio" | "João Claudio" | "JC13" |

    Scenario Outline:  Apagar usuario cadastrado na lista de usuarios
        Given acesso a pagina de cadastro de usuarios
        When clico no botao apagar ao lado do usuario <USUARIO>
        Then nao devo visualizar o usuario <USUARIO> na lista de usuarios cadastrados

        Examples:
        | USUARIO       | NOME           | SENHA  |
        | "joaoClaudio" | "João Claudio" | "JC13" |
