# Construção de Testes automatizados E2E com Cypress e Cucumber e Allure Reports.


# Plugins utilizados:

- https://github.com/TheBrainFamily/cypress-cucumber-preprocessor
- https://github.com/Shelex/cypress-allure-plugin

# Pré-requisitos:
- Possuir Node.js instalado;
- Possuir Cypress instalado;

# Instalação 

1. Instalação Node.js: https://nodejs.org/en/download/
2. Instalação Cypress: https://docs.cypress.io/guides/getting-started/installing-cypress#System-requirements
3. Instalação cucumber-preprocessor: https://github.com/TheBrainFamily/cypress-cucumber-preprocessor#installation
4. Instalação Allure Reports: https://github.com/Shelex/cypress-allure-plugin#installation
